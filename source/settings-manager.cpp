#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <vector>
#include <filesystem>
namespace fs = std::filesystem;

#include <settings-manager/settings-manager.h>
#include <BLRevive/Utils.h>
#include <BLRevive/BLRevive.h>
using namespace BLRE;
using namespace BLRE::Detours;

void jsonBinaryToTArray(nlohmann::json::binary_t b, TArray<unsigned char>& tarr)
{
    tarr.Data = std::move(b.data());
    tarr.Count = tarr.Max = b.size();
}

void from_json(const nlohmann::json& j, FSettingsData& setting)
{
    setting.Type = j["Type"];
    setting.Value1 = j["Value1"];

    switch (setting.Type)
    {
    case ESettingsDataType::SDT_Int32:
    case ESettingsDataType::SDT_UInt32:
    case ESettingsDataType::SDT_Float:
        setting.Value2.Dummy = 0;
        break;
    case ESettingsDataType::SDT_Int64:
    case ESettingsDataType::SDT_Double:
    case ESettingsDataType::SDT_DateTime:
        setting.Value2.Dummy = j["Value2"].get<int>();
        break;
    case ESettingsDataType::SDT_Blob:
        if (setting.Value1) {
            uint8_t* data = (uint8_t*)std::malloc(setting.Value1 * sizeof(uint8_t));
            auto bin = j["Value2"]["bytes"].get<std::vector<uint8_t>>();
            memcpy(data, bin.data(), setting.Value1 * sizeof(uint8_t));
            setting.Value2.Dummy = (int)data;
        }
    case ESettingsDataType::SDT_String:
        // @todo fix string parsing with nlohmann::json
        if (setting.Value1) {
            /*std::string str(j["Value2"]);

            FString* fstr = new FString(str.c_str());
            setting.Value2.Dummy = (int)fstr->Data;*/
        }
        break;
    }
}

void to_json(nlohmann::json& j, const FSettingsData setting)
{
    j["Type"] = setting.Type;
    j["Value1"] = setting.Value1;

    switch (setting.Type)
    {
    case ESettingsDataType::SDT_Int32:
    case ESettingsDataType::SDT_UInt32:
    case ESettingsDataType::SDT_Float:
        j["Value2"] = 0;
        break;
    case ESettingsDataType::SDT_Int64:
    case ESettingsDataType::SDT_Double:
    case ESettingsDataType::SDT_DateTime:
        j["Value2"] = setting.Value2.Dummy;
        break;
    case ESettingsDataType::SDT_Blob:
        if (setting.Value1) {
            std::vector<uint8_t> data;
            data.assign((uint8_t*)setting.Value2.Dummy, ((uint8_t*)setting.Value2.Dummy) + setting.Value1);
            j["Value2"] = nlohmann::json::binary(data);
        }
    case ESettingsDataType::SDT_String:
        if (setting.Value1) {
            //j["Value2"] = (wchar_t*)(setting.Value2.Dummy);
        }
        break;
    }
}

template<typename T>
std::vector<T> TArrayToVector(const TArray<T> arr)
{
    std::vector<T> v;
    v.assign(arr.Data, arr.Data + arr.Count);
    return v;
}

void SettingsManager::Init()
{
    this->profileName = BLRevive::GetInstance()->URL.GetParam("Name", "default");
    this->profileDirPath = fs::path(Utils::FS::BlreviveConfigPath()) / fmt::format("settings_manager_{}", profileName);

    this->LoadSettings();

    auto dtRegisterCustomDataStores = FunctionDetour::Create("FoxGame.FoxPC.RegisterCustomPlayerDataStores", this, &SettingsManager::RegisterCustomPlayerDataStoresCb);
    dtRegisterCustomDataStores->Enable();

    auto dtSettingsApplyChanges = FunctionDetour::Create("FoxGame.FoxSettingsUIBase.ei_ApplyChanges", this, &SettingsManager::SettingsApplyChangesDt);
    dtSettingsApplyChanges->Enable();

    auto dtVideoApplyChanges = FunctionDetour::Create("FoxGame.FoxSettingsUIVideo.ei_ApplyChanges", this, &SettingsManager::SettingsApplyChangesDt);
    dtVideoApplyChanges->Enable();

    auto dtKillGameOnQuitMatch = FunctionDetour::Create("FoxGame.FoxMenuUI.ei_QuitMatch", this, &SettingsManager::HackQuitGameOnQuit);
    dtKillGameOnQuitMatch->Enable();

    auto dtKillGameOnIdleKicked = FunctionDetour::Create("FoxGame.FoxPC.ClientKickedForIdle", this, &SettingsManager::HackQuitGameOnIdleKick);
    dtKillGameOnIdleKicked->Enable();
}

std::vector<FOnlineProfileSetting> SettingsManager::LoadProfileSettings()
{
    fs::path pfsfPath = profileDirPath / ProfileFileName;
    this->Log->debug("loading profile settings from {}", Utils::FS::Relative(pfsfPath).string());

    try {
        return Utils::ReadJsonFile<std::vector<FOnlineProfileSetting>>(pfsfPath.string());
    }
    catch (nlohmann::json::exception& ex) {
        this->Log->error("failed to parse keybindings file")({
            {"Path", Utils::FS::Relative(pfsfPath).string()},
            {"Error", ex.what()}
            });
    }
    catch (std::exception ex) {
        this->Log->error("failed to load profile file")({
            {"Path", Utils::FS::Relative(pfsfPath).string()},
            {"Error", ex.what()}
        });
    }
    return {};
}

void SettingsManager::LoadSettings()
{
    auto path = Utils::FS::Relative(this->profileDirPath).string();

    if (!fs::exists(profileDirPath))
        fs::create_directories(profileDirPath);

    if (!fs::exists(profileDirPath / ProfileFileName))
    {
        auto defaultProfileSettings = UObject::GetDefaultInstanceOf<UFoxProfileSettingsPC>();
        profileSettings = TArrayToVector(defaultProfileSettings->DefaultSettings);
        SaveProfileSettings();
    }
    else {
        this->Log->debug("loading settings from {}", path);
        profileSettings = LoadProfileSettings();
    }

    this->Log->info("loaded settings from {}", path);
}

void SettingsManager::SaveProfileSettings()
{
    auto pfsPath = profileDirPath / ProfileFileName;
    try {
        Utils::WriteJsonFile(pfsPath.string(), profileSettings);
    }
    catch (std::exception ex) {
        this->Log->error("failed to write profile settings")({
            {"Path", Utils::FS::Relative(pfsPath).string()},
            {"Error", ex.what()}
            });
    }
}

void SettingsManager::SaveSettings()
{
    this->Log->debug("saving settings");
    this->SaveProfileSettings();
    this->Log->debug("saved settings");
}

void SettingsManager::ApplyProfileSettings(UFoxProfileSettings* pfs)
{
    for (auto& setting : pfs->ProfileSettings)
    {
        for (const auto profSetting : profileSettings)
        {
            if (setting.ProfileSetting.PropertyId == profSetting.ProfileSetting.PropertyId)
            {
                setting.ProfileSetting.Data.Value1 = profSetting.ProfileSetting.Data.Value1;
                setting.ProfileSetting.Data.Value2 = profSetting.ProfileSetting.Data.Value2;
            }
        }
    }
}

void SettingsManager::UpdateProfileSettings(UFoxProfileSettings* profSettings)
{
    profileSettings = TArrayToVector(profSettings->ProfileSettings);
}

void DETOUR_CB_CLS_IMPL(SettingsManager::RegisterCustomPlayerDataStoresCb, AFoxPC, RegisterCustomPlayerDataStores, pc)
{
    detour->Continue();

    Log->debug("applying profile settings");

    if (!pc->ProfileSettings)
    {
        Log->debug("skip applying settings since AFoxPC::ProfileSettings is null");
        return;
    }

    ApplyProfileSettings(pc->ProfileSettings);
    pc->ApplyAllSettings();
}

void DETOUR_CB_CLS_IMPL(SettingsManager::SettingsApplyChangesDt, UFoxSettingsUIBase, ei_ApplyChanges, settingsBase)
{
    detour->Continue();
    this->UpdateProfileSettings(settingsBase->ProfileSettings);
    this->SaveProfileSettings();
}

// Possibly sloppy implementation of the temporary hacks in 4ff77e73b0ee3cef96d0693fc31ec4bcf0697804
void DETOUR_CB_CLS_IMPL(SettingsManager::HackQuitGameOnQuit, UFoxMenuUI, ei_QuitMatch, settingsBase)
{
    this->Log->warn("hack: exiting the game on event FoxMenuUI ei_QuitMatch to not disconnect the player joined before you");
    exit(0);
}

void DETOUR_CB_CLS_IMPL(SettingsManager::HackQuitGameOnIdleKick, AFoxPC, ClientKickedForIdle, settingsBase)
{
    this->Log->warn("hack: exiting the game on event FoxPC ClientKickedForIdle to not disconnect the player joined before you");
    exit(0);
}

/// <summary>
/// Module initializer (function must exist and export demangled!)
/// </summary>
/// <param name="data"></param>
extern "C" __declspec(dllexport) void InitializeModule(BLRE::BLRevive* blre)
{
    if (Utils::IsServer()) {
        return;
    }
    
    SettingsManager* sm = new SettingsManager();
    sm->Init();
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved)
{
	return true;
}