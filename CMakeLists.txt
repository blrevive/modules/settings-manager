cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

project(
    settings-manager
    VERSION 0.0.1
    LANGUAGES CXX
)

# ---- Include guards ----
if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
    message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.")
endif()

include(cmake/cpm.cmake)
CPMAddPackage(
  NAME blrevive-common
  GIT_REPOSITORY https://gitlab.com/blrevive/common
  VERSION 0.0.4
)

# create dll
configure_blrv_environment(settings-manager ${CMAKE_CURRENT_SOURCE_DIR})
set_target_properties(settings-manager PROPERTIES CXX_STANDARD 20)